function filterBy(arr, itemType) {
  return (arr.filter( item => typeof item !== itemType ));
}

let array = ["hello", "world", 23, "23", false, null, undefined]

console.log(filterBy(array, "string"));
console.log(filterBy(array, "number"));
console.log(filterBy(array, "boolean"));
console.log(filterBy(array, "null"));
console.log(filterBy(array, "undefined"));